export default interface User {
  id?: number;
  login: string;
  name: string;
  password: string;
  createAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
